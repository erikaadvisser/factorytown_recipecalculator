import java.io.File
import java.math.BigDecimal


data class World(
    val items: List<Item>,
    val buildings: List<Building>,
    val recipes: List<Recipe>,
) {
    fun item(name: String): Item {
        return items.find { it.name == name } ?: error("No item with name `${name}`")
    }

    fun recipe(item: Item): Recipe {
        return recipes.find { it.mainOutput == item } ?: error("No recipe for ${item.name}")
    }
}


val world = createWorld()


fun createWorld(): World {

    val buildings = readBuildings()
    val items = readItems()
    val recipes = readRecipes(items, buildings)

    return World(items, buildings, recipes)
}

//private fun createRecipes(items: List<Item>, buildings: List<Building>): List<Recipe> {
//
//    val grain = items.find { it.name == "Grain" }!!
//    val feed = items.find { it.name == "Animal feed" }!!
//    val farm = buildings.find { it.name == "Farm" }!!
//    val foodMill = buildings.find { it.name == "Food mill" }!!
//
//    val grainRecipe = Recipe(grain, emptyList(), listOf(Output(grain, BigDecimal(1))), BigDecimal(2), farm)
//    val feedRecipe =
//        Recipe(feed, listOf(Input(grain, BigDecimal(2))), listOf(Output(feed, BigDecimal(1))), BigDecimal(2), foodMill)
//
//    return listOf(grainRecipe, feedRecipe)
//}

//private fun createItems(): List<Item> {
//    val grain = Item("Grain", null)
//    val feed = Item("Animal feed", null)
//    val flour = Item("Flour", SellStats(CoinType.YELLOW, 4, 4, 1, 30, 45))
//    return listOf(grain, feed)
//}

//private fun createBuildings(): List<Building> {
//    val farm = Building("Farm", BigDecimal.ZERO, BigDecimal.ONE, 10)
//    val foodMill = Building("Food mill", POINT_FIVE, POINT_FIVE, 5)
//
//    return listOf(farm, foodMill)
//}


private fun readItems(): List<Item> {
    val lines = File("src/world/items.csv").readLines(Charsets.US_ASCII).drop(1)

    return lines.map { line: String ->
        val parts = line.split(",")

        val name = parts[0].trim().lowercase()
        val sellStats = getSellStats(parts)

        Item(name, sellStats)
    }
}

private fun getSellStats(parts: List<String>): SellStats? {
    if (parts.size == 1) {
        return null
    }

    val coinType = parts[1].trim().uppercase()
    val townBuilding = parts[7].trim().uppercase()

    return SellStats(
        CoinType.valueOf(coinType),
        parts[2].trim().toInt(),
        parts[3].trim().toInt(),
        parts[4].trim().toInt(),
        parts[5].trim().toInt(),
        parts[6].trim().toInt(),
        TownBuilding.valueOf(townBuilding),
    )
}

private fun readBuildings(): List<Building> {
    val lines = File("src/world/buildings.csv").readLines(Charsets.US_ASCII).drop(1)

    return lines.map { line: String ->
        val parts = line.split(",")

        val name = parts[0].trim().lowercase()
        val type = parts[1].trim().uppercase()

        when (type) {
            "RAW" -> {
                Building(name, BigDecimal.ZERO, BigDecimal.ONE, 10)
            }
            "PROCESS" -> {
                Building(name, POINT_FIVE, POINT_FIVE, 5)
            }
            "PASTURE" -> {
                Building(name, TWO, POINT_FIVE, 5)
            }
            "FUEL" -> {
                Building(name, BigDecimal.TEN, BigDecimal.ONE, 1)
            }
            "WELL" -> {
                Building(name, BigDecimal.TEN, BigDecimal.ONE, 1)
            }
            else -> error("Unknown building type: '${type}' for building: ${name}")
        }
    }
}

private fun readRecipes(items: List<Item>, buildings: List<Building>): List<Recipe> {
    val lines = File("src/world/recipes.csv").readLines(Charsets.US_ASCII)

    val iterator = lines.iterator()
    val recipes = ArrayList<Recipe>()
    var building = buildings.first()

    while (iterator.hasNext()) {
        val line = next(iterator) ?: break
        if (line.trim().lowercase().startsWith("building")) {
            val parts = line.split(":")
            val buildingName = parts[1].trim().lowercase()
            building = buildings.find { it.name == buildingName } ?: error("Unknown building in recipe: '${buildingName}', line: $line")
            continue
        }

        val recipe = readRecipe(line, iterator, items, building)
        if (recipe != null) {
            recipes.add(recipe)
        }
    }
    return recipes
}

fun readRecipe(line: String, iterator: Iterator<String>, items: List<Item>, building: Building): Recipe? {
    val parts = line.split(",")
    val mainOutputName = parts[0].trim().lowercase()
    val mainOutputItem = items.find { it.name == mainOutputName } ?: error("Unknown item in recipe: '${mainOutputName}', line: $line")
    val mainOutputAmount = parts[1].trim().toInt()
    val mainOutput = Output(mainOutputItem, BigDecimal(mainOutputAmount))

    val work = BigDecimal(parts[2].trim())

    val inputs = ArrayList<Input>()
    val outputs = ArrayList<Output>(listOf(mainOutput))

    while (iterator.hasNext()) {
        val line = iterator.next()

        if (line.isBlank()) {
            break
        }

        if (line.startsWith("-")) {
            inputs.add(processInput(line, items))
        }
        if (line.startsWith("+")) {
            outputs.add(processOutput(line, items))
        }
        if (!line.startsWith("-") && !line.startsWith("+")) {
            error("Unrecognized line: ${line}")
        }
    }

    return Recipe(mainOutputItem, inputs, outputs, work, building)
}

fun next(iterator: Iterator<String>): String? {
    do {
        val line = iterator.next()
        if (line.isNotBlank() && !line.startsWith("/")) {
            return line
        }
    } while (iterator.hasNext())
    return null

}

fun processInput(line: String, items: List<Item>): Input {
    val pair = process(line, items)
    return Input(pair.first, pair.second)
}

fun processOutput(line: String, items: List<Item>): Output {
    val pair = process(line, items)
    return Output(pair.first, pair.second)
}

fun process(line: String, items: List<Item>): Pair<Item, BigDecimal> {
    val genericPart = line.substring(1).trim()
    val parts = genericPart.split(",")
    val itemName = parts[0].trim().lowercase()
    val item = items.find { it.name == itemName } ?: error("No item found with name: '${itemName}' problem in line: ${line} ")
    val amount = BigDecimal(parts[1].trim())

    return Pair(item, amount)
}