import java.math.BigDecimal
import java.math.MathContext
import java.math.RoundingMode


fun main(args:Array<String>) {
//   calculateRecipe("butter", BigDecimal("1.25"))
   calculateRecipe("Berry cake", BigDecimal("1"))
}

fun calculateRecipe(itemNameInput: String, throughPut: BigDecimal) {
   val itemName= itemNameInput.trim().lowercase()
   val item = world.item(itemName)


   val totalBuildingWork:List<BuildingWork> = calculateRecipe(item, throughPut)
   println("Total buildings")

   val grouped : Map<Building, List<BuildingWork>> = totalBuildingWork.groupBy { it.building }

   var totalPop: Int = 0

   grouped.forEach{ building: Building, works: List<BuildingWork> ->
      val totalWork:BigDecimal = works.sumOf { it.work }

      val buildingRequirement = calcPopRequired(building, totalWork)

      println("${buildingRequirement.number} x ${building} - pop: ${buildingRequirement.pop}")
      totalPop += buildingRequirement.pop
   }

   println( "Total pop: ${totalPop}")

   if ( item.sellStats != null) {
      val houseHappiness = BigDecimal(item.sellStats.happinessDuration) * throughPut

      val happinessPerPop = houseHappiness * BigDecimal(item.sellStats.happinessAmount) / BigDecimal(totalPop)

      val coinsTotal = throughPut * BigDecimal(item.sellStats.coinAmount)
      val coinsPerPop = coinsTotal /  BigDecimal(totalPop)

      val coinsConsumption = throughPut * BigDecimal(item.sellStats.coinDuration) / TWO



      println ("\nHouses that you can keep happy: ${houseHappiness} ")
      println ("Minimum houses to allow consumption: ${coinsConsumption} ")
      println ("Coins/s: ${coinsTotal}")
      println ("\nHappiness per pop: ${happinessPerPop}")
      println ("Coins per pop: ${coinsPerPop}")

      println("\n${item},${totalPop},${happinessPerPop},${coinsTotal},${happinessPerPop},${coinsPerPop},${item.sellStats.coinType}")
   }
}

fun calcPopRequired(building: Building, workRequired: BigDecimal): BuildingRequirement {
   val maxWorkSingleBuilding = building.baseWork + BigDecimal(building.maxWorkers) * building.workPerWorker

   val buildings = (workRequired / maxWorkSingleBuilding).setScale(0, RoundingMode.CEILING)

   val workAllBuildingsMaxPop = buildings * maxWorkSingleBuilding

   val overWork = workAllBuildingsMaxPop - workRequired;

   val workersNotNeeded = (overWork / building.workPerWorker).toInt()

   val totalWorkers = buildings.toInt() * building.maxWorkers - workersNotNeeded

   return BuildingRequirement(buildings.toInt(), totalWorkers)
}


fun calculateRecipe(item: Item, throughPut: BigDecimal): List<BuildingWork> {
   val recipe = world.recipe(item)

   println("${item}: ${throughPut}/s via ${recipe.mainOutput} recipe")

   val mainOutput: Output = recipe.outputs.find{ it.item == item}!!
   val outputPerCycle = mainOutput.amount

   val workPerCycle = recipe.work / outputPerCycle
   val work = workPerCycle * throughPut

   val cyclesPerSecond = work / recipe.work

   val thisBuildingWork = BuildingWork(recipe.building, work)
   println(" + ${thisBuildingWork.building} ${cyclesPerSecond} c/s, work: ${work} ")

   val inputsRequired = ArrayList<Rate>()

   recipe.inputs.forEach { input: Input ->
      val inThroughPut = input.amount * throughPut
      println("    - In  ${input.item}: ${inThroughPut}/s")

      inputsRequired.add( Rate(input.item, inThroughPut))
   }
   recipe.outputs.forEach { output: Output ->
      val outThroughPut = output.amount * throughPut
      println("    - Out ${output.item}: ${outThroughPut}/s")
   }

   val buildingWorks:List<BuildingWork> = inputsRequired.flatMap { inputRequired: Rate ->
      calculateRecipe(inputRequired.item, inputRequired.throughPut)
   }

   return buildingWorks.plus(thisBuildingWork)  // totalBuildingWork
}