import java.math.BigDecimal


enum class CoinType { YELLOW, RED, BLUE, PURPLE }

data class Item(
    val name: String,
    val sellStats: SellStats?
) {
    override fun toString(): String {
        return name
    }
}

data class SellStats(
    val coinType: CoinType,
    val coinAmount: Int,
    val experiencePoints: Int,
    val happinessAmount: Int,
    val coinDuration: Int,
    val happinessDuration: Int,
    val townBuilding: TownBuilding,
)

data class Building(
    val name: String,
    val baseWork: BigDecimal,
    val workPerWorker: BigDecimal,
    val maxWorkers: Int,
) {
    override fun toString(): String {
        return name
    }
}

enum class TownBuilding {
    FOOD_MARKET, TAVERN, GENERAL_STORE, APOTHECARY, SCHOOL, SPECIALTY_GOODS
}

data class Recipe(
    val mainOutput: Item,
    val inputs: List<Input>,
    val outputs: List<Output>,
    val work: BigDecimal,
    val building: Building,
)

data class Input(
    val item: Item,
    val amount: BigDecimal,
)

data class Output(
    val item: Item,
    val amount: BigDecimal,
)

data class Rate(
    val item: Item,
    val throughPut: BigDecimal,
)


data class BuildingWork(
    val building: Building,
    val work: BigDecimal,
)

data class BuildingRequirement(
    val number: Int,
    val pop: Int,
)
