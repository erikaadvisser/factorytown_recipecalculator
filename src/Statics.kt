import java.math.BigDecimal

val POINT_FIVE = BigDecimal("0.5")
val TWO = BigDecimal("2")
val TWO_POINT_FIVE = BigDecimal("2.5")

